# Portail des outils


Le portail des outils du réseau Relief est construit grâce à l'outil *Pad2portail*.
Il permet de référencer et centraliser les différents outils déployés par le réseau Relief.


## Apercu
Les pages du site sont générées par du contenu Markdown récupéré sur l'instance hedgedoc `https://pad.relief-aura.fr/`. Le site est ensuite construit via hugo et déployé par Gitlab Pages.


### Prérequis
* [hugo](https://gohugo.io/)
* [node.js](https://nodejs.org/en)


## Déploiement
Le site est hébergé par le service Gitlab Pages.


### Configuration
La configuration du pipeline se fait dans le fichier [`.gitlab-ci.yml`](./.gitlab-ci.yml).
- Ce fichier s'exécute à chaque *push* sur ce dépôt.
- Il contient aussi les dépendances à installer.
- Le site est accessible à cette adresse : *(lien à mettre)*


### Regénérer le site
Le site peut être actualisé et modifié. Pour le reconstruire une fois les modifications effectuées il faut relancer la pipeline par un "push".
Pour se faire, il suffit de créer cliquer sur "regénérer le site" en bas à droite des pages.
Ce bouton relance la pipeline via le script [`pipeline.js`](./themes/pad2web/static/js/pipeline.js)


## Construction
Le site est construit via le générateur de site [hugo](https://gohugo.io/).
Cette construction repose sur un thème personnalisé *pad2web*.
Les configurations des pages sont définies dans le dossier [`themes/pad2web/layouts`](./themes/pad2web/layouts/).
* Par défaut, les pages sont définies par le fichier [`_default/baseof.html`](./themes/pad2web/layouts/_default/baseof.html).
* Les pages simples (les pages qui ne sont pas des pages type "listes") sont elles spécifiées dans le fichier [`_default/single.html`](./themes/pad2web/layouts/_default/single.html).
* Les différents éléments comme *head*, *header* et *footer* sont définis dans le dossier [`partials`](./themes/pad2web/layouts/partials/)
* La page d'accueil est elle définie dans le fichier [`ìndex.html`](./themes/pad2web/layouts/index.html) à la racine


### La page accueil
Le fichier [`ìndex.html`](./themes/pad2web/layouts/index.html) génère donc la page d'accueil à partir de l'ensemble des fichiers markdown placés dans le dossier [`./content/outils/`](./content/outils/).


```go
{{ range (where .Site.RegularPages "Section" "outils") }}
    {{ partial "page-render.html" . }}
{{ end }}
```


Comme indiqué dans les lignes ci-dessus, les pages sont affichées à partir du fichier [`partial/page-render.html`](./themes/pad2web/layouts/partial/page-render.html). L'affichage des pages est généré à partir de leur variable définie dans l'en-tête des pads (la partie en yml). Les variables sont ensuite appelées lors de la construction du site :


```go
<h2>{{ .Params.title }}</h2>
```


Par défaut, les variables `title`, `lien`, `description`, et `type` sont nécessaires à la construction.
* `title`
Afin d'afficher le titre de la page
* `lien`
Il contient le lien de l'outil déployé et permet d'ouvrir l'outil
* `description`
Il permet d'afficher la description de la page
* `type`
Cette variable permet d'afficher le logo de l'usage proposé par l'outil (en format svg).
Ces logos sont stockés dans la banque de logo [`partial/logo/`](./themes/pad2web/layouts/partials/logo/). Actuellement il y 9 logos disponible : chat, cloud, compta, forum, network, pad, table, visio, et wiki.


### CSS
L'aspect visuel du site est généré par le fichier CSS [`style.css`](./themes/pad2web/static/css/style.css).


### BaseURL
Pour assurer la correspondance des liens dans toutes les pages, la variable `baseURL`est définie dans le dossier [`config.yaml`](./config.yaml). Cette variable sert à définir l'url de base du site. Pour un site hébergé par Gitlab Page, cette url de base vaut par défaut `https://nom-utilisateur.gitlab.io/nom-du-projet/`


## Contenu
Le contenu markdown qui sert de matière à construction pour Hugo se trouve dans le dossier [`content/`](./content/).


### Récupération des pads
Le contenu des pages des outils est récupéré sur des pads hedgedoc. Cette requête est définie dans le fichier [`fetch_file.js`](./fetch_file.js)


Ce fichier lance une requête pour récupérer les contenus des pads resencés à l'adresse `https://pad.relief-aura.fr/portail-liste-outils`. Pour changer de pad de référence il suffit de changer la variable `HEDGEDOC_URL`


```js
const HEDGEDOC_URL = 'https://pad.relief-aura.fr/portail-liste-outils';
```


### Chemin des fichiers
Le contenu des pads référencés est ensuite écrit dans le dossier [`content/outils`](./content/outils/).


Pour changer le dossier où sont contenus les fichiers permettant de générer la page d'accueil, il faut changer les chemins définis :


* lors de l'écriture dans le fichier [`fetch_file.js`](./fetch_file.js).


```js
const writer = await fs.promises.writeFile(`content/outils/${filename}.md`, markdown);
```


* lors de la génération du site dans le fichier [`ìndex.html`](./themes/pad2web/layouts/index.html).


```go
{{ range (where .Site.RegularPages "Section" "outils") }}
```


### Url
L'url de la page du site est construite à partir de l'url du pad correspondant.
La partie de l'url du pad après `https://pad.relief-aura.fr` servira à construire l'url de la page du site.


L'écriture de l'url est définie  à partir de l'url de base du site + la localisation du fichier + le nom du fichier
* pour l'url de base du site, il s'agit de l'url de la page d'accueil (voir aussi [baseURL](#baseurl)).
* pour la localisation du fichier, voir [la partie précédente](#chemin-des-fichiers).
* le nom du fichier est définie par la variable `filename` dans le fichier [`fetch_file.js`](./fetch_file.js). Par défaut le nom du fichier correspond à la dernière partie de l'url après le `-` si il y en a, ou après le `/` sinon.


```js
const filename = link.split('/').pop().split("-")[1];
```


Ainsi, pour récapituler prenons l'exemple d'un pad avec l'url `https://pad.relief-aura.fr/outils-nextcloud` par défaut, les fichiers sont stockés dans le dossier `outils`. L'adresse de base du site est `portail.relief-aura.fr`. L'url de la page générée sera donc `portail.relief-aura.fr/outils/Nextcloud`.


### Modifier/ajouter des pads
Pour ajouter des pads ou modifier il suffit de se rendre sur le [pads portail](https://pad.relief-aura.fr/portail-liste-outils) qui référence les autres portails.
Ensuite, il suffit de cliquer sur un lien pour modifier le contenu du pad ou alors d'ajouter un lien pour créer une nouvelle page.
Pour ajouter un page il est conseillé de partir d'un copier coller d'une page précédante pour ne pas perdre le formatage.

**Avant de modifier les pads, il est important :**
* de lire la [documentation du paramétrage Hedgedoc de l'outils *pad2portail*](https://pad.relief-aura.fr/outils-documentation)
* d'avoir lu et compris [les différentes variables](#la-page-accueil)
* d'avoir lu et compris la [création des urls](#url)