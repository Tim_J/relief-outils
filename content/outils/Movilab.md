---
title: Movilab
type: wiki
lien: https://movilab.org/wiki/R%C3%A9seau_Tiers-Lieux_AuRA
description: Le wiki du réseau pour documenter ses actions et les partager
---
# Movilab

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/bc09dfbb-99e8-49cf-85bf-e5d137ea9d47.png)
> Capture d'écran du portail relief sur Movilab

## Movilab n'est pas un wiki
Movilab c'est une méthodologie de documentation qui permet de documenter et de garder trace de ce qui est expérimenter localement pour pertmettre à d'autres personnes de s'inspirer de de readpater le commun documenter.

Movilab c'est aussi un wiki, comme Wikipédia, qui permet de construire un captial informationel commun des Tiers-Lieux. Ce wiki est accessible et ouvert au contributions de toutes et tous.

## Pour prendre en main Movilab
Prendre en main Movilab