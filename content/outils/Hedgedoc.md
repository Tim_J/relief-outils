---
title: Hedgedoc
type: pad
lien: https://pad.relief-aura.fr
description: Les pads, pour prendre des notes en ligne à plusieurs
---

# Hedgedoc

![capture d'écran de l'application Hedgedoc, à droite en noire, la partie éditable, et à gauche en blanc la partie visible](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/0d3c2e38-1e31-47ba-85ce-be23fd783089.png)
> capture d'écran de l'application Hedgedoc, à droite en noire, la partie éditable, et à gauche en blanc la partie visible

## C'est quoi HedgeDoc ?
HedgeDoc est un outil de prise de note. L'application permet de prendre des notes **simutltanément**, **à plusieurs**, **en ligne**, et **sans avoir à créer de compte** (plusieurs niveau de partage des droits est possible).
Pour résumé, c'est une alternative à **Google doc**.

## A savoir
### Le markdown
La particularité de ce logiciel de prise de note (appelé *pad*) est son format de prise de note, **le Markdown**.
Pour faire simple, le markdown c'est l'ajout de différents caractères entre les mots pour les mettres en valeurs et leur attribuer une forme particulière. Par exemple deux axtérix `**` autour d'un mot le mette en **gras**. Un hashtag `#` devant une ligne la transforme en gros titre... 

### Apperçu
L'interface HedgeDoc se compose de deux parties :
- La partie éditable à gauche (ou en cliquant sur l'icone crayon en haut) est la partie modifiable, en markdown
- La partie visible à droite (ou en cliquant sur l'oeil en haut) est la partie visible du document avec le contenu mis en forme sommairement

### Les plus
A noter aussi qu'un historique des versions permets de restauré des versions différentes et de suivre les modifications. Il est de plus possible d'exporter sa prise de note en markdown ou html pour en faire autre chose (un site, un diapo...)

## Aller plus loin

Pour en savoir plus :
- [Un tutoriel Markdown](https://pad.lamyne.org/utiliser-codimd#)
- [Les différentes fonctionnalités disponible](https://pad.relief-aura.fr/features#)