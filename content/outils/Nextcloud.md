---
title: Nexcloud
type: cloud
lien: https://nuage.relief-aura.fr/
description: Le nuage, pour stocker ses fichiers, s'organiser et bien plus
---

# Nexcloud

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/0f40f528-009b-447c-81c2-808a70852592.png)
> Page d'acceuil du Nextcloud de Relief 

## Pour faire simple

C'est la plateforme de stockage du réseau. Il est possible d'y charger des documents et de télécharger ou visualiser ceux déjà là.

## Les différentes fonctionnalités
Mais Nextcloud est aussi une plateforme avec de nombreuses options. Notamment :
- Un agenda partagé
- Un espace de gestion de projet (type Kanban) appelé Deck
- Un espace de gestion de contact
- Bien plus encore, à découvrir en fouillant l'outil

Pour accéder à ces différentes fonctionnalité, il suffit de cliquer sur les icones dans la barre de navigation en haut.

