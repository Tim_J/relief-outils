---
title: Rocketchat
type: chat
lien: https://chat.tiers-lieux.org/channel/RELIEF_reseau_TL_AuRA
description: Le chat, pour échanger entre pairs et s'organiser entre commission
---

# Rocketchat

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/fde15b12-cf9d-4fea-ba63-9c5d673d242e.png)
> Capture d'écran du canal d'acceuil sur le Rocket chat

## En bref
C'est un outil de **communication** type chat **libre** et **open source**. Il se compose de différents cannaux thématiques pour échanger et converser plus efficacement entre les différents membres du réseau. 
A noter que le chat n'est pas un message de stockage ni de documentation. Les discussions se perde facilement et ne sont pas faites pour garder trace.

## Les canaux 
Chaque commision à son canal pour échanger de ses avancées. Le canal de base est le canal `RELIEF_reseau_TL_AuRA`. Ici, les nouvelles et nouveaux peuvent **se présenter**, poser des questions et se faire (re)diriger vers les autres canaux.
Des messages sont afficher en haut des canaux soit pour expliquer l'objectif du canal, soit pour mettre en lien des supports travail.

## Les plus
Le chat de Relief est aussi **connecté** à celui plus général **des Tiers-Lieux** ([chat.tiers-lieux.org](chat.tiers-lieux.org)). Pas besoin de recréer un compte. L'organisation des différents canaux permet d'échanger sur des sujets Relief ou plus généraux.