---
title: Discourse
type: forum
lien: https://forum.tiers-lieux.org/c/reseaux-locaux/auvergne-rhone-alpes/39
description: Le forum des tiers-lieux, pour débatre et discuter
---

# Discourse 

C'est la brique logiciel derière le forum des Tiers-Lieux.

C'est un espace ouvert en ligne, une agora où échanger et débattre de différents sujets qui anime l'univers des Tiers-Lieux