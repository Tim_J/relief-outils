const fs = require('fs');

const HEDGEDOC_URL = 'https://pad.relief-aura.fr/portail-liste-outils';

async function fetchLinks() {
    const urls = await await fetch(HEDGEDOC_URL+"/download")
    const liste = await urls.text();
    return liste;
}

fetchLinks().then(liste => {
    console.log("le markdown de base: " + liste);
    const links = extractLinks(liste);
    console.log("les liens extraits: " + links);
    downloadLinks(links).then(() => {
      console.log("Tous les fichiers ont été téléchargés.");
    }).catch(error => {
      console.error("Erreur lors du téléchargement des fichiers: " + error);
    });
  }).catch(error => {
    console.error("Erreur lors de la récupération du markdown: " + error);
  });


  function extractLinks(liste) {
    const regex = /\[.*?\]\((.*?)\)/g;
    const links = [];
    let match;
    while ((match = regex.exec(liste))) {
      links.push(match[1]);
    }
    return links;
  }

  
  async function downloadLinks(links) {
    for (const link of links) {
      try {
        const filename = link.split('/').pop().split("-")[1];
        const content = await fetch(link+"/download")
        const markdown = await content.text();
        const writer = await fs.promises.writeFile(`content/outils/${filename}.md`, markdown);
      }catch (error) {
        console.error(error);
      }
    }
  }